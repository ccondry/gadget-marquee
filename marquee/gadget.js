var finesse = finesse || {};

/** @namespace */
finesse.modules = finesse.modules || {};
finesse.modules.marqueeGadget = (function ($) {
  var clientLogs;
  /**
  * Utility function that returns an array of key-value pairs
  * for the query parameters in a given URL.
  */
  var getUrlVars = function(url) {
    var vars = {};
    var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi,
    function(m,key,value) {
      vars[key] = value;
    });
    return vars;
  }

  /**
  * Validates that the gadget is configured with the correct query params from the desktop layout.
  * MRD ID is required for the gadget to work. For the MRD name and max dialogs we default to 'Media'
  * and 5 dialogs respectively if they are not configured or misconfigured.
  */
  var checkGadgetQueryParams = function() {
    console.log('marqueeGadget checkGadgetQueryParams')
    //First get just the URI for this gadget out of the full finesse URI and decode it.
    var gadgetURI = decodeURIComponent(getUrlVars(location.search)["url"]);

    //Now get the individual query params from the gadget URI
    var decodedGadgetURI = getUrlVars(gadgetURI);
    var message = decodedGadgetURI["message"];
    console.log('custom marquee set in URL = ', message)
    // set marquee message using gadget xml url parameter
    if (message && message.length) {
      $('#marquee').html(decodeURIComponent(message))
    } else {
      // leave marquee default
    }
  }

  /** @scope finesse.modules.TaskManagementGadget */
  return {
    /**
    * Performs all initialization for this gadget
    */
    init : function () {
      console.log('init marquee gadget')
      var config = finesse.gadget.Config;
      clientLogs = finesse.cslogger.ClientLogger;   // declare clientLogs

      /** Initialize private references */
      // utils = finesse.utilities.Utilities;
      // msgs = finesse.utilities.I18n.getString;
      // uiMsg = finesse.utilities.MessageDisplay;
      // adjustGadgetHeight();

      checkGadgetQueryParams()

      // Initiate the ClientServices and load the user object.  ClientServices are
      // initialized with a reference to the current configuration.
      finesse.clientservices.ClientServices.init(config);

      clientLogs.init(gadgets.Hub, "marqueeGadget"); //this gadget id will be logged as a part of the message
      user = new finesse.restservices.User({
        id: config.id
      });

      // Initiate the ContainerServices and add a handler for when the tab is visible
      // to adjust the height of this gadget in case the tab was not visible
      // when the html was rendered (adjustHeight only works when tab is visible)

      containerServices = finesse.containerservices.ContainerServices.init();
      containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.ACTIVE_TAB, function(){
        clientLogs.log("Gadget is now visible");  // log to Finesse logger
      });
      containerServices.makeActiveTabReq();

      //now that the gadget has loaded, remove the loading indicator
      gadgets.loadingindicator.dismiss();
    }
  };
}(jQuery))
